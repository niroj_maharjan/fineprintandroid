package com.zeronebits.fingerprint.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.daimajia.numberprogressbar.NumberProgressBar;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.thin.downloadmanager.ThinDownloadManager;
import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.InfiniteScrollAdapter;
import com.yarolegovich.discretescrollview.Orientation;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;
import com.zeronebits.fingerprint.BuildConfig;
import com.zeronebits.fingerprint.DiscreteScrollViewOptions;
import com.zeronebits.fingerprint.R;
import com.zeronebits.fingerprint.adapter.ShopAdapter;
import com.zeronebits.fingerprint.adapter.ViewPagerAdapterBookDetail;
import com.zeronebits.fingerprint.database.DatabaseController;
import com.zeronebits.fingerprint.entity.Book;
import com.zeronebits.fingerprint.entity.Image;
import com.zeronebits.fingerprint.tab.SlidingTabLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import static java.lang.Thread.sleep;


/**
 * Created by neero on 26/03/2017.
 */
public class BookDetails extends AppCompatActivity implements DiscreteScrollView.OnItemChangedListener, GoogleApiClient.OnConnectionFailedListener {
    private static final String TAG = "fsdfdsfdsffdsf";
    CallbackManager callbackManager;

    //    CustomPagerAdapter mCustomPagerAdapter;
    ViewPager mViewPager, slidingTabViewPager;
    DatabaseController databaseController;
    ArrayList<Book> bookArrayList;
    ViewPagerAdapterBookDetail viewPagerBookDetail;
    CharSequence Titles[] = {"About The Book", "About The Author"};
    int Numboftabs = 2;
    SlidingTabLayout tabs;
    Book book;
    Button buyButton;
    SignInButton googleSignIn;

    private ThinDownloadManager downloadManager;
    int downloadId1;
    private static final int DOWNLOAD_THREAD_POOL_SIZE = 4;
    NumberProgressBar numberProgressBar;
    LinearLayout mainLayout;
    ImageView blur_back_first,blur_back_second;
    private DiscreteScrollView itemPicker;
    private InfiniteScrollAdapter infiniteAdapter;
    Dialog d;
    LoginButton loginButton;
    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 007;
    SharedPreferences sharedPreferences;
    int position;
    Button facebookLogin,googleLogin;
    Button changeList;
    LinearLayout mainBlur;

    private static final int REQUEST_CODE_PAYMENT = 1;
    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_SANDBOX;
    // note that these credentials will differ between live & sandbox environments.
    private static final String CONFIG_CLIENT_ID = "AWTagDdijYVHeKZ8x45lac8ZHJtGBtGuAP5u4rZX72sH4zzkLe4jMIWjZc9ql63ws4k7kmhEMVRTp83O";
    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(CONFIG_CLIENT_ID)
            // The following are only used in PayPalFuturePaymentActivity.
            .merchantName("FinePrint")
            .merchantPrivacyPolicyUri(Uri.parse("https://www.example.com/privacy"))
            .merchantUserAgreementUri(Uri.parse("https://www.example.com/legal"));


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.book_detail);
        databaseController = new DatabaseController(this);
        bookArrayList = databaseController.getBookList();
//        mCustomPagerAdapter = new CustomPagerAdapter(BookDetails.this, bookArrayList);
        Bundle bundle = getIntent().getExtras();
        position = bundle.getInt("position");
        Log.d("jsdhfsdjkf", position + "   kdfj");
        findViews();
        slidingTabViewPager = (ViewPager) findViewById(R.id.bookdetailViewPager);
        book = bookArrayList.get(position);
        setTabValue(position);
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                smoothScrollToUserSelectedPosition(position);
            }
        }, 500);
        printHashKey();
//        setValue(book);
    }


    public void setTabValue(int position) {
        viewPagerBookDetail = new ViewPagerAdapterBookDetail(getSupportFragmentManager(), Titles, Numboftabs, position);
        // Assigning ViewPager View and setting the adapter
        slidingTabViewPager.setAdapter(viewPagerBookDetail);
        // Assiging the Sliding Tab Layout View
        tabs = (SlidingTabLayout) findViewById(R.id.tabs);
        tabs.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width
        tabs.setViewPager(slidingTabViewPager);
    }


    public void smoothScrollToUserSelectedPosition(int position) {
        Log.d("sdjhfsdkjfhsdfsdjhf", "here     " + position);
        int destination = infiniteAdapter.getClosestPosition(position);
        Log.d("sdjhfsdkjfhsdfsdjhf", destination + " ");
        itemPicker.smoothScrollToPosition(destination);

    }

    private void findViews() {
        buyButton = (Button) findViewById(R.id.buy_button);
        mainLayout = (LinearLayout) findViewById(R.id.main_layout);
        blur_back_first = (ImageView) findViewById(R.id.blur_back_first);
        blur_back_second = (ImageView) findViewById(R.id.blur_back_second);
        mainBlur = (LinearLayout) findViewById(R.id.main_blur);
        changeList = (Button) findViewById(R.id.change_list);
        itemPicker = (DiscreteScrollView) findViewById(R.id.item_picker);
        itemPicker.setOrientation(Orientation.HORIZONTAL);
        itemPicker.addOnItemChangedListener(this);
        infiniteAdapter = InfiniteScrollAdapter.wrap(new ShopAdapter(bookArrayList, this));
        itemPicker.setAdapter(infiniteAdapter);


        itemPicker.setItemTransitionTimeMillis(DiscreteScrollViewOptions.getTransitionTime());
        itemPicker.setItemTransformer(new ScaleTransformer.Builder()
                .setMinScale(0.8f)
                .build());
        buyButton.performClick();

        buyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sharedPreferences = getSharedPreferences("kotha", MODE_PRIVATE);
                String email = sharedPreferences.getString("email", "");
                if (email.equalsIgnoreCase("")) {
                    loginDialog();
                } else {
                    buyDialog();
                }
                d.show();
               /*BookAndAuthorDescription.
                Blurry.with(BookDetails.this).radius(1).sampling(30).capture(mainLayout).into(blur_back_first);
                mainBlur.bringToFront();
                mainBlur.setVisibility(View.VISIBLE);*/
               /*Intent i = new Intent(BookDetails.this, Web.class);
                startActivity(i);*/
//                buyDialog();

            }
        });


//        smoothScrollToUserSelectedPosition(position);

    }

    public void printHashKey() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(BuildConfig.APPLICATION_ID, PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.i(TAG, "printHashKey() Hash Key: " + hashKey);
            }
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, "printHashKey()", e);
        } catch (Exception e) {
            Log.e(TAG, "printHashKey()", e);
        }
    }


    public void buyDialog() {
        d = new Dialog(BookDetails.this);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.setContentView(R.layout.buy_dialog);
        ImageView im = (ImageView)d.findViewById(R.id.im_paypal_payment);
        d.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(final DialogInterface arg0) {
                Log.d("dsadsad", "here");
                mainBlur.setVisibility(View.GONE);
            }
        });

        d.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                mainBlur.setVisibility(View.GONE);
            }
        });
        im.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                payPalPayment();
            }
        });
    }
    private PayPalPayment getThingToBuy(String paymentIntent) {
        return new PayPalPayment(new BigDecimal("5.01"), "USD", "sample item",
                paymentIntent);
    }


    public void payPalPayment(){
        PayPalPayment thingToBuy = getThingToBuy(PayPalPayment.PAYMENT_INTENT_SALE);

        /*
         * See getStuffToBuy(..) for examples of some available payment options.
         */
        Intent intent = new Intent(BookDetails.this, PaymentActivity.class);
        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);

        startActivityForResult(intent, REQUEST_CODE_PAYMENT);
    }





    public void loginDialog() {
        d = new Dialog(BookDetails.this);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.setContentView(R.layout.login_dialog);
        d.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(final DialogInterface arg0) {
                Log.d("dsadsad", "here");
                mainBlur.setVisibility(View.GONE);
            }
        });

        d.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                mainBlur.setVisibility(View.GONE);
            }
        });
        facebookLogin = (Button) d.findViewById(R.id.facebook_login);
        googleLogin = (Button) d.findViewById(R.id.btn_google_sign_in);
        loginButton = (LoginButton) d.findViewById(R.id.login_button);

        loginButton.setBackgroundResource(R.drawable.facebook);
        loginButton.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
        loginButton.setCompoundDrawablePadding(0);
        loginButton.setPadding(0, 0, 0, 0);
        loginButton.setText("");
        loginButton.setReadPermissions("public_profile email");
        googleSignIn = (SignInButton) d.findViewById(R.id.google_sign_in);
        googleSignIn = (SignInButton) d.findViewById(R.id.google_sign_in);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (AccessToken.getCurrentAccessToken() != null) {
                    RequestData();
                    d.dismiss();
                }
            }
        });

        facebookLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginButton.performClick();
            }
        });
        googleLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                googleSignIn.performClick();
            }
        });
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                if (AccessToken.getCurrentAccessToken() != null) {
                    RequestData();
                }
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException exception) {
            }
        });
        googleSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                btnSignIn.performClick();
                googleLogin();
                signIn();
                d.dismiss();
            }

        });

    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    public void onCurrentItemChanged(@Nullable RecyclerView.ViewHolder viewHolder, int adapterPosition) {
        int positionInDataSet = infiniteAdapter.getRealPosition(adapterPosition);
//        setValue(bookArrayList.get(positionInDataSet));
        setTabValue(positionInDataSet);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    public void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    public void googleLogin() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            mGoogleApiClient.stopAutoManage(BookDetails.this);
            mGoogleApiClient.disconnect();
        }catch (Exception e){

        }
    }
    public void RequestData() {
        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                JSONObject json = response.getJSONObject();
                try {
                    if (json != null) {
                        SharedPreferences.Editor editor = getSharedPreferences("kotha", MODE_PRIVATE).edit();
                        editor.putString("name", json.getString("name"));
                        editor.putString("email", json.getString("email"));
                        editor.putString("link", json.getJSONObject("picture").getJSONObject("data").getString("url"));
                        Log.e(TAG, "display name fb: " + json.getJSONObject("picture").getJSONObject("data").getString("url"));

                        editor.commit();
                        Toast.makeText(BookDetails.this, "Done", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,link,email,picture");
        request.setParameters(parameters);
        request.executeAsync();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();


            String personName = acct.getDisplayName();
            String personPhotoUrl = acct.getPhotoUrl().toString();
            String email = acct.getEmail();
            Log.e(TAG, "display name: " + personPhotoUrl);

            SharedPreferences.Editor editor = getSharedPreferences("kotha", MODE_PRIVATE).edit();
            editor.putString("name", personName);
            editor.putString("email", email);
            editor.putString("link", personPhotoUrl);
            editor.commit();

        } else {

        }
    }
}
