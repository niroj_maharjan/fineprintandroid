package com.zeronebits.fingerprint.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.zeronebits.fingerprint.R;
import com.zeronebits.fingerprint.adapter.MyBooksAdapter;
import com.zeronebits.fingerprint.database.DatabaseController;
import com.zeronebits.fingerprint.entity.Book;

import java.util.ArrayList;

/**
 * Created by neero on 23/06/2017.
 */

public class MyBooks extends AppCompatActivity {

    ArrayList<Book> books;
    GridView bookList;
    DatabaseController databaseController;
    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_books);
        setUpToolBar();
        bookList = (GridView)findViewById(R.id.book_list);
        databaseController = new DatabaseController(this);
        books = databaseController.getBookList();

        MyBooksAdapter myBooksAdapter = new MyBooksAdapter(this,R.layout.my_books_single_item,books);
        bookList.setAdapter(myBooksAdapter);
    }


    public void setUpToolBar(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.back);
        getSupportActionBar().setTitle("My Books");
    }
}
