package com.zeronebits.fingerprint.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.zeronebits.fingerprint.R;

/**
 * Created by neero on 06/06/2017.
 */

public class Web extends AppCompatActivity{

    WebView mWebview;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mWebview  = new WebView(this);

        mWebview.getSettings().setJavaScriptEnabled(true); // enable javascript
        mWebview.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        mWebview.setPadding(10,50,10,10);
       /* mWebview.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            }
        });*/

        mWebview.loadUrl("http://www.zeronebits.com/epub/");
        setContentView(mWebview);


    }
}
