package com.zeronebits.fingerprint.activities;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;

import com.zeronebits.fingerprint.BuildConfig;
import com.zeronebits.fingerprint.Dashboard;
import com.zeronebits.fingerprint.DrawerActivity;
import com.zeronebits.fingerprint.R;
import com.zeronebits.fingerprint.entity.Gallery;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by neero on 31/03/2017.
 */
public class SplashScreen extends AppCompatActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                Intent mainIntent = new Intent(SplashScreen.this,Dashboard.class);
                startActivity(mainIntent);
                finish();
            }
        }, 3*1000);


        try {
            PackageInfo info = getPackageManager().getPackageInfo(BuildConfig.APPLICATION_ID,
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }
}
