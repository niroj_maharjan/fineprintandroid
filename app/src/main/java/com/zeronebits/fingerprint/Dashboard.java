package com.zeronebits.fingerprint;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.florent37.materialviewpager.MaterialViewPager;
import com.github.florent37.materialviewpager.header.HeaderDesign;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.zeronebits.fingerprint.activities.MyBooks;
import com.zeronebits.fingerprint.fragment.RecyclerViewFragment;
public class Dashboard extends DrawerActivity {
    private MaterialViewPager mViewPager;
    private Toolbar toolbar;
    SharedPreferences sharedPreferences;
    CircularImageView circleImageView;
    ImageView dpImage;
    TextView home,myBooks,news,aboutUs,termsAndCondition;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        circleImageView = (CircularImageView)findViewById(R.id.profile_picture);
        setTitle("");
        sharedPreferences = getSharedPreferences("kotha", MODE_PRIVATE);
        String dp = sharedPreferences.getString("link", "");

        clickListener();

        if(!dp.equalsIgnoreCase("")){
            Log.d("fdsfdsfdsf",dp + "    6");
            Picasso.with(this).load(dp).into(circleImageView);
        }
        mViewPager = (MaterialViewPager) findViewById(R.id.materialViewPager);
        toolbar = mViewPager.getToolbar();

        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }
//        setupToolbar();
        mViewPager.getViewPager().setAdapter(new FragmentStatePagerAdapter(getSupportFragmentManager()) {

            @Override
            public Fragment getItem(int position) {
                switch (position % 4) {
                    default:
                        Fragment fragment = null;
                        fragment = new RecyclerViewFragment();
                        Bundle args = new Bundle();
                        args.putInt("position",position);
                        fragment.setArguments(args);
                        return fragment;
                }
            }

            @Override
            public int getCount() {
                return 3;
            }

            @Override
            public CharSequence getPageTitle(int position) {
                switch (position % 3) {
                    case 0:
                        return "Popular";
                    case 1:
                        return "Top Seller";
                    case 2:
                        return "Something";
                }
                return null;
            }
        });

        mViewPager.setMaterialViewPagerListener(new MaterialViewPager.Listener() {
            @Override
            public HeaderDesign getHeaderDesign(int page) {
                switch (page) {
                    case 0:
                        return HeaderDesign.fromColorResAndDrawable(
                                R.color.black,
                                getResources().getDrawable(R.drawable.buda));
                    case 1:
                        return HeaderDesign.fromColorResAndDrawable(
                                R.color.black,
                                getResources().getDrawable(R.drawable.buda));
                    case 2:
                        return HeaderDesign.fromColorResAndDrawable(
                                R.color.black,
                                getResources().getDrawable(R.drawable.buda));


                    //execute others actions if needed (ex : modify your header logo)
                    default:

                        return HeaderDesign.fromColorAndDrawable(Color.parseColor("#227736"), getResources().getDrawable(R.drawable.buda));
                }
            }
        });

        mViewPager.getViewPager().setOffscreenPageLimit(mViewPager.getViewPager().getAdapter().getCount());
        mViewPager.getPagerTitleStrip().setViewPager(mViewPager.getViewPager());
    }

    private void clickListener() {
        home = (TextView) findViewById(R.id.home);
        myBooks = (TextView) findViewById(R.id.myBooks);
        news = (TextView) findViewById(R.id.news);
        aboutUs = (TextView) findViewById(R.id.aboutUs);
        termsAndCondition = (TextView) findViewById(R.id.termsAndcondition);
        Typeface regular = Typeface.createFromAsset(getAssets(),"TITILLIUMWEB-REGULAR.TTF");

        home.setTypeface(regular);
        myBooks.setTypeface(regular);
        news.setTypeface(regular);
        aboutUs.setTypeface(regular);
        termsAndCondition.setTypeface(regular);

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(Dashboard.this, "Home", Toast.LENGTH_SHORT).show();
//                getSupportFragmentManager().beginTransaction().replace(R.id.main_activity_content_frame,new MyBooks()).commit();
            }
        });


        myBooks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                getSupportFragmentManager().beginTransaction().replace(R.id.main_activity_content_frame,new MyBooks()).commit();
                Intent i = new Intent(Dashboard.this, MyBooks.class);
                startActivity(i);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        return true;
    }
}
