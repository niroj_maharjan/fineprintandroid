package com.zeronebits.fingerprint.entity;

import com.zeronebits.fingerprint.R;

import java.util.Arrays;
import java.util.List;

/**
 * Created by yarolegovich on 16.03.2017.
 */

public class Gallery {

    public static Gallery get() {
        return new Gallery();
    }

    private Gallery() {
    }

    public List<Image> getData() {
        return Arrays.asList(
                new Image(R.drawable.place_holder),
                new Image(R.drawable.place_holder),
                new Image(R.drawable.place_holder),
                new Image(R.drawable.place_holder),
                new Image(R.drawable.place_holder),
                new Image(R.drawable.place_holder));
    }
}
