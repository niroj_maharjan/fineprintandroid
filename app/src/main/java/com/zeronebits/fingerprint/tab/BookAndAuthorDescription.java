package com.zeronebits.fingerprint.tab;

/**
 * Created by neero on 29/03/2017.
 */
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zeronebits.fingerprint.R;
import com.zeronebits.fingerprint.activities.BookDetails;
import com.zeronebits.fingerprint.database.DatabaseController;
import com.zeronebits.fingerprint.entity.Book;

import java.util.ArrayList;

import jp.wasabeef.blurry.Blurry;

/**
 * Created by hp1 on 21-01-2015.
 */
public class BookAndAuthorDescription extends Fragment {


    DatabaseController databaseController;
    ArrayList<Book> bookArrayList;
    TextView releaseDate, language, tags, pages, bookName, authorName;
    public LinearLayout viewpagerMainLayout;
    ImageView mainBlur;
    TextView description;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v =inflater.inflate(R.layout.book_author_description,container,false);
        Bundle bundle = this.getArguments();
        findViewById(v);
        int position = bundle.getInt("position");
        int bookPosition = bundle.getInt("bookPosition");
        databaseController = new DatabaseController(getActivity());
        bookArrayList = databaseController.getBookList();

        Book book = bookArrayList.get(bookPosition);

        if(position == 0){
            description.setText(book.getBookDescription());
        }else{
            description.setText(book.getPublisherName());
        }
        setValue(book);
        return v;
    }

    private void findViewById(View v) {
        Typeface bold = Typeface.createFromAsset(getActivity().getAssets(),"TITILLIUMWEB-BOLD.TTF");
        Typeface regular = Typeface.createFromAsset(getActivity().getAssets(),"TITILLIUMWEB-REGULAR.TTF");
        Typeface semibold = Typeface.createFromAsset(getActivity().getAssets(),"TITILLIUMWEB-SEMIBOLD.TTF");

        releaseDate = (TextView) v.findViewById(R.id.release_date);
        language = (TextView) v.findViewById(R.id.language);
        tags = (TextView)v.findViewById(R.id.tags);
        pages = (TextView) v.findViewById(R.id.pages);
        bookName = (TextView) v.findViewById(R.id.bookName);
        authorName = (TextView) v.findViewById(R.id.authorName);
        viewpagerMainLayout = (LinearLayout) v.findViewById(R.id.main_layout);
        description = (TextView)v.findViewById(R.id.description);

        bookName.setTypeface(bold);
        authorName.setTypeface(semibold);
        tags.setTypeface(regular);
        pages.setTypeface(regular);
        language.setTypeface(regular);
        releaseDate.setTypeface(regular);
        description.setTypeface(regular);
    }


    public void setValue(Book book) {
        releaseDate.setText(book.getPublishDate());
        language.setText("Nepali");
        tags.setText(book.getBookEdition());
        pages.setText(book.getBookPages());
        bookName.setText(book.getBookName());
        authorName.setText(book.getPublisherName());
    }


}
