package com.zeronebits.fingerprint.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.zeronebits.fingerprint.R;
import com.zeronebits.fingerprint.entity.Book;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by florentchampigny on 24/04/15.
 */
public class TestRecyclerViewAdapter extends RecyclerView.Adapter<TestRecyclerViewAdapter.MyViewHolder> {

    List<Object> contents;

    static final int TYPE_HEADER = 0;
    static final int TYPE_CELL = 1;
    private ArrayList<Book> bookArrayList;
    Context context;

    public TestRecyclerViewAdapter(ArrayList<Book> bookArrayList, Context context) {
        this.bookArrayList = bookArrayList;
        this.context = context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title,releaseDate,language,type,amount;
        ImageView bookImage;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
//            authorName = (TextView) view.findViewById(R.id.author_name);
            releaseDate = (TextView) view.findViewById(R.id.release_date);
            language = (TextView) view.findViewById(R.id.language);
            type = (TextView) view.findViewById(R.id.type);
            amount = (TextView) view.findViewById(R.id.amount);

            bookImage = (ImageView)view.findViewById(R.id.bookImage);

        }
    }


    @Override
    public int getItemCount() {
        return bookArrayList.size();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_item_detail, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Book book = bookArrayList.get(position);
        holder.title.setText(book.getBookName());
//        holder.authorName.setText(book.getPublisherName());
        holder.releaseDate.setText(book.getPublishDate());
        holder.language.setText("Nepali");
        holder.type.setText(book.getBookEdition());
        holder.amount.setText("Nrs "+book.getBookPrice());

        Typeface bold = Typeface.createFromAsset(context.getAssets(),"TITILLIUMWEB-BOLD.TTF");
        Typeface regular = Typeface.createFromAsset(context.getAssets(),"TITILLIUMWEB-REGULAR.TTF");
        Typeface semibold = Typeface.createFromAsset(context.getAssets(),"TITILLIUMWEB-SEMIBOLD.TTF");

        holder.title.setTypeface(bold);
//        holder.authorName.setTypeface(semibold);
        holder.amount.setTypeface(bold);
        holder.releaseDate.setTypeface(regular);
        holder.language.setTypeface(regular);
        holder.type.setTypeface(regular);
        Picasso.with(context).load(book.getBookPhoto()).resize(100,200).into(holder.bookImage);

    }

}