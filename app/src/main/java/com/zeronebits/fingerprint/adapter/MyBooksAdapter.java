package com.zeronebits.fingerprint.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.zeronebits.fingerprint.R;
import com.zeronebits.fingerprint.entity.Book;

import java.util.ArrayList;

/**
 * Created by neero on 23/06/2017.
 */

public class MyBooksAdapter extends ArrayAdapter<Book> {

    Context context;
    ArrayList<Book> books;
    public MyBooksAdapter(@NonNull Context context, @LayoutRes int resource,ArrayList<Book> books) {
        super(context, resource);
        this.context = context;
        this.books = books;

    }


    @Override
    public int getCount() {
        return books.size();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.my_books_single_item, parent, false);
        Book book = books.get(position);
        ImageView bookImage = (ImageView)rowView.findViewById(R.id.bookImage);
        TextView bookName = (TextView)rowView.findViewById(R.id.bookName);
        Picasso.with(context).load(book.getBookPhoto()).resize(100,200).into(bookImage);

        return rowView;

    }
}
