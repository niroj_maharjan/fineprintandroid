package com.zeronebits.fingerprint.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;
import com.zeronebits.fingerprint.R;
import com.zeronebits.fingerprint.entity.Book;

import java.util.ArrayList;

/**
 * Created by neero on 30/03/2017.
 */
public class CustomPagerAdapter extends PagerAdapter {

    Context mContext;
    LayoutInflater mLayoutInflater;
    int imageSelectedPosition;
    ArrayList<Book> bookArrayList;

    public void setImageSelectedPosition(int imageSelectedPosition) {
        this.imageSelectedPosition = imageSelectedPosition;
    }

    public CustomPagerAdapter(Context context,ArrayList<Book> bookArrayList) {
        mContext = context;
        this.bookArrayList = bookArrayList;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return bookArrayList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.imageview_viewpager, container, false);
        Book book = bookArrayList.get(position);
        ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);
        Picasso.with(mContext).load(book.getBookPhoto()).into(imageView);
         /*   if(position == imageSelectedPosition){
                imageView.setScaleX(1.2f);
                imageView.setScaleY(1.2f);
            }else{
                imageView.setScaleX(0.8f);
                imageView.setScaleY(0.8f);
            }*/


        container.addView(itemView);


        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}

