import zipfile
from lxml import etree
import sys,os, random, struct
from Crypto.Cipher import AES
import hashlib

print("trying")

def unzip(fname):
    zip_ref = zipfile.ZipFile(fname, 'r')
    newpath = r'C:\Users\Own\Desktop\en\check' 
    if not os.path.exists(newpath):
        os.makedirs(newpath)
    zip_ref.extractall(newpath)
    zip_ref.close()
    searchfile(newpath)
    
    
def searchfile(src):
    for root, dirs, files in os.walk(src):
        for file in files:
            if file.endswith(".html"):
                print(os.path.join(root, file))
                key = '0123456789abcdef'
                encrypt_file(key,os.path.join(root, file),os.path.join(root, file)+'_')
            if file.endswith(".xhtml"):
                print(os.path.join(root, file))
                key = '0123456789abcdef'
                encrypt_file(key,os.path.join(root, file),os.path.join(root, file)+'_')            
    zipFileEpub(src, src)

def encrypt_file(key, in_filename, out_filename=None, chunksize=64*1024):
    """ Encrypts a file using AES (CBC mode) with the
        given key.

        key:
            The encryption key - a string that must be
            either 16, 24 or 32 bytes long. Longer keys
            are more secure.

        in_filename:
            Name of the input file

        out_filename:
            If None, '<in_filename>.enc' will be used.

        chunksize:
            Sets the size of the chunk which the function
            uses to read and encrypt the file. Larger chunk
            sizes can be faster for some files and machines.
            chunksize must be divisible by 16.
    """
    if not out_filename:
        out_filename = in_filename + '.enc'

    iv = 16 * '\x00'
    encryptor = AES.new(key, AES.MODE_CBC, iv)
    filesize = os.path.getsize(in_filename)

    with open(in_filename, 'rb') as infile:
        with open(out_filename, 'wb') as outfile:
            outfile.write(struct.pack('<Q', filesize))
            outfile.write(iv)
            #outfile.write(bytes(iv, 'UTF-8'))

            while True:
                chunk = infile.read(chunksize)
                if len(chunk) == 0:
                    break
                elif len(chunk) % 16 != 0:
                    chunk += b' ' * (16 - len(chunk) % 16)

                outfile.write(encryptor.encrypt(chunk))
    os.remove(in_filename)
    os.rename(out_filename, in_filename)            
                #outfile.write(bytes(encryptor.encrypt(chunk), 'UTF-8'))
                

def zipFileEpub(src, dst):
    zf = zipfile.ZipFile("%s.epub" % ("check__"), "w", zipfile.ZIP_DEFLATED)
    abs_src = os.path.abspath(src)
    for dirname, subdirs, files in os.walk(src):
        for filename in files:
            absname = os.path.abspath(os.path.join(dirname, filename))
            arcname = absname[len(abs_src) + 1:]
            zf.write(absname, arcname)
    zf.close()                
                
def main():
    filename1 = sys.argv[-1]
    print (filename1)
    unzip(filename1)
    print("done")


if __name__ == '__main__':
    main()